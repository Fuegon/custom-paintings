[![](https://cf.way2muchnoise.eu/title/545417.svg)](https://www.curseforge.com/minecraft/texture-packs/custom-paintings)

# Custom paintings resource pack
This Minecraft resourcepack adds some new paintings by using the CustomModelData feature added in 1.14.

It is inspired by the custom paintings used on the Hermitcraft server, as seen in [this video (14:53)](https://youtu.be/OBOAlfX7Zus?t=893).

## Installation
### Modrinth
You can download the resource pack from [Modrinth](https://modrinth.com/resourcepack/custom-paintings).

### CurseForge
You can download the resource pack from [CurseForge](https://www.curseforge.com/minecraft/texture-packs/custom-paintings).

### This repo
#### Releases
Visit the [releases page for this repository](https://gitlab.com/Fuegon/custom-paintings/-/releases) to get the last released version of the resource pack. 

#### Working version
Download this repository as a zip file. Place the zip file inside the resourcepack folder in your Minecraft installation.

## Usage
The following image shows the paintings available in the resourcepack. You can also add your own.

![Available paintings](legend.png)

To get the paintings you have two options: using the _give command_ or using _mukitanuki's Custom Roleplay Data datapack_.

### Give command
Use the following command to give the player the custom painting defined by the CustomModelData number.

`/give @p painting{CustomModelData:<number>}`

Where __\<number>__ is the painting index as defined in painting.json

### mukitanuki's Custom Roleplay Data datapack

Alternatively, and more survival friendly, CurseForge user mukitanuki has created a datapack to change an items CustomModelData tag. You can download it from [CurseForge](https://www.curseforge.com/minecraft/customization/custom-roleplay-data-datapack).

Once installed you can run the command `/trigger CustomModelData set <number>` changing __\<number>__ for the index of the painting.

Check the CurseForge page for complete instructions on this datapack

## Adding custom paintings
Paintings in this resourcepack are composed of 16 by 16 pixels tiles, although the texture can be upscaled.

### Textures
Place your textures (in png format) into `assets\minecraft\textures\item\paintings` directory inside the resource pack. They can also be placed inside their own subdirectory to keep them organized.

### Models
Create a new .json file for each painting you want to add in `assets\minecraft\models\item\paintings`. You can also place this files inside of their own subdirectory.

Copy and paste the next code into the file you just created and change the text between quotes after the following properties:

    {
        "parent": "item/paintings/master_painting",
        "credit": "YOUR_NAME",
        "texture_size": [16, 16],
        "textures": {
            "0": "item/paintings/YOUR_TEXTURE",
            "1": "painting/back",
            "particle": "item/paintings/YOUR_TEXTURE"
        }
    }

- "credit": Change _YOUR\_NAME_ for the message to credit you with this painging.
- "texture_size": Change this numbers if your painting is not 16 by 16 pixels.
- "0" and "particle": Change _YOUR\_TEXTURE_ for the name of your texture withouth the `.png` extensions. If you placed the textures inside of a directory you also have to include it.

### Adding models to the painting
Edit `assets\minecraft\models\item\painting.json` and add a comma after the last _predicate_ line and paste the following code.

    {"predicate": {"custom_model_data":MY_INDEX}, "model": "item/paintings/MY_MODEL"}

Change: 
- _MY\_INDEX_: for the index of the new painting, make sure they are not repeated or it will override another painting.
- _MY\_MODEL_: change my model for the name of the `.json` file you created in the previous step, without the file extensions. If you placed your file inside of a directory remember to include it as well.

## Credits
First 4 sample paintings in the resource pack are created from art released under CC0 license by ansimuz. You can find the original images at the next links:

- Painting 1 "River" - [Top Down River - Shooter Environment by ansimuz](https://opengameart.org/content/top-down-river-shooter-environment)
- Painting 2 "Forest" - [Forest Background  by ansimuz](https://opengameart.org/content/forest-background)
- Paintings 3 and 4 "Mountain" - [Mountain at Dusk Background by ansimuz](https://opengameart.org/content/mountain-at-dusk-background)
- Paintings 5 to 10 "Road" - [Country Side Platform Tiles by ansimuz](https://opengameart.org/content/country-side-platform-tiles)

The rest of the paintings are created based on public domain images. You can find the originals in the next links:

- Paintings 11 to 14 "Port" - [Cumulus Clouds, East River by Robert Henri](https://commons.wikimedia.org/wiki/File:Cumulus_Clouds,_East_River_SAAM-1992.91_1.jpg)
- Paintings 15 to 26 "Bridge" - [Florence by Henryk Dietrich](https://artvee.com/dl/florence-3/)
- Paintings 27 to 30 "Sunset" - [Sunset by Frederic Edwin Church](https://artvee.com/dl/sunset-13/)
- Paintings 31 to 32 "Fairy Nebula" - [The Fairy of Eagle Nebula by NASA, ESA, and The Hubble Heritage Team (STScI/AURA)](https://commons.wikimedia.org/wiki/File:Fairy_of_Eagle_Nebula.jpg)
- Paintings 33 to 36 "Cone Nebula" - [Cone Nebula/NGC 2264 by NASA, H. Ford (JHU), G. Illingworth (UCSC/LO), M.Clampin (STScI), G. Hartig (STScI), the ACS Science Team, and ESA](https://commons.wikimedia.org/wiki/File:Cone_Nebula_(NGC_2264)_Star-Forming_Pillar_of_Gas_and_Dust.jpg)
- Painting 37 "Flowers" - [Still Life with Flowers in a Glass Vase by Jan Davidsz. de Heem](https://www.rijksmuseum.nl/en/collection/SK-C-214)
- Paintings 38 to 41 "Temple" - [brown high-rise building by Lucrezia Carnelos](https://unsplash.com/photos/8XR23JaBUuA)
- Painting 42 "Sun" - [After the Deluge by George Frederic Watts](https://commons.wikimedia.org/wiki/File:Watts_%E2%80%93_After_the_Deluge.jpg)
- Paintings 43 to 54 "Ruins" - [Chimborazo Volcano by Frederic Edwin Church](https://artvee.com/dl/chimborazo-volcano/)
- Paintings 55 to 63 "Pandemonium" - [Pandemonium by John Martin](https://commons.wikimedia.org/wiki/File:John_Martin_Le_Pandemonium_Louvre.JPG)
- Paintings 64 to 69 "Heimweg" - [Auf dem Heimweg II by Anna Klein ](https://commons.wikimedia.org/wiki/File:Anna_Klein_12_Auf_dem_Heimweg_II.jpg)
