# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- "Heimweg" painting

## [1.5.0] - 2023-09-24
### Added
- "Ruins" painting
- "Pandemonium" painting

### Changed
- Updated pack_format for 1.20.2 with multiformat tags

## [1.4.0] - 2023-03-26
### Added
- "Temple" painting
- "Sun" painting

### Changed
- Updated pack_format to 13 for 1.19.4.

## [1.3.0] - 2023-01-18
### Added
- "Flower" painting.

### Changed
- Updated pack_format to 12 for 1.19.3.
- Changed border and back of paintings

## [1.2.0] - 2022-06-12
### Added
- This changelog.
- "Fairy Nebula" painting.
- "Cone Nebula" painting.

### Changed
- Added Curseforge badge to README.md.
- Updated pack_format to 9 for 1.19.

## [1.1.0] - 2022-05-29
### Added
- "Port" painting.
- "Bridge" painting.
- "Sunset" painting.

## [1.0.0] - 2021-12-16
### Added
- Painting base model.
- "River" painting.
- "Forest" painting.
- "Mountain" painting.
- "Road" painting.

[Unreleased]: https://gitlab.com/Fuegon/custom-paintings
[1.3.0]: https://gitlab.com/Fuegon/custom-paintings/-/releases/1.3.0
[1.2.0]: https://gitlab.com/Fuegon/custom-paintings/-/releases/1.2.0
[1.1.0]: https://gitlab.com/Fuegon/custom-paintings
[1.0.0]: https://gitlab.com/Fuegon/custom-paintings